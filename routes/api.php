<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')
    ->group(function(Router $router) {
        $router->post('views/articles/{article}', 'ArticleController@addView');
        // Get library by category
        $router->get('categories/{category}/libraries', 'CategoryController@libraries');
        // Get youtube videos
        $router->get('histories', 'YoutubeController@index');
        // Get categories
        $router->get('categories', 'CategoryController@index');
        // Articles
        $router->get('articles', 'ArticleController@index');
        // Get articles by category
        $router->get('categories/{category}/articles', 'CategoryController@articles');
        // Get the article
        $router->get('articles/{article}', 'ArticleController@show');
        // Get contacts
        $router->get('contacts', 'ContactController@index');
        // Get regions by contact
        $router->get('contacts/{contact}/regions', 'ContactController@regions');
        // Get centers by contact and region
        $router->get('contacts/{contact_id}/regions/{region_id}/centers', 'ContactController@centers');
        // Get centers by region
        $router->get('regions/{region_id}/centers', 'RegionController@centers');
        // Get gallery
        $router->get('galleries/{region_id?}', 'GalleryController@index');
        // Requests
        $router->post('requests', 'RequestController@store');
        $router->post('requests/phone', 'RequestController@phone');
        $router->get('requests/email', 'RequestController@email');
        // Regions
        $router->get('regions', 'RegionController@index');
        // Region
        $router->get('regions/{region}', 'RegionController@show');

        $router->get('/settings/{setting}', 'SettingsController@setting');

        $router->post('onesignal/player-ids', 'OneSignalController@store');
        $router->post('onesignal/player-ids/{player_id}', 'OneSignalController@destroy');
    });
