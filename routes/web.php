<?php

use App\OneSignalPlayer;
use App\Events\ArticleCreated;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('lang/{locale}', 'LocalizationController@index');

Route::get('/', 'MainController@index')->name('main');
Route::get('news', 'NewsController@index')
    ->name('news');
Route::get('mass-medias', 'MassMediaController@index')->name('mass-medias');
Route::get('articles/{article}', 'ArticleController@show')->name('article');
Route::get('libraries', 'LibraryController@index')->name('libraries');
Route::get('galleries', 'GalleryController@index')->name('galleries');
Route::get('about', 'AboutController@index')->name('about');
Route::get('contacts', 'ContactController@index')->name('contacts');
Route::get('galleries/date/{date}/title/{title}', 'GalleryController@show');
Route::get('histories', 'HistoryController@index')->name('histories');
Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


Route::get('/test', function () {
    $player_ids = OneSignalPlayer::select('player_id')->get()->pluck('player_id');
    event(new ArticleCreated($player_ids, 'Новая статья!', ['article' => 'ART']));
});