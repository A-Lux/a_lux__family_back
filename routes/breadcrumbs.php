<?php

use Illuminate\Support\Facades\Lang;

Breadcrumbs::for('main', function ($trail) {
    $trail->push(Lang::get('interface.main'), route('main'));
});

Breadcrumbs::for('mass-medias', function ($trail) {
    $trail->parent('main');

    $trail->push(Lang::get('interface.mass_media'), route('mass-medias'));
});

Breadcrumbs::for('contacts', function ($trail) {
    $trail->parent('main');

    $trail->push(Lang::get('interface.contacts'), route('contacts'));
});

Breadcrumbs::for('galleries', function ($trail) {
    $trail->parent('main');

    $trail->push(Lang::get('interface.gallery'), route('galleries'));
});

Breadcrumbs::for('libraries', function ($trail) {
    $trail->parent('main');

    $trail->push(Lang::get('interface.library'), route('libraries'));
});

Breadcrumbs::for('news', function ($trail) {
    $trail->parent('main');

    $trail->push(Lang::get('interface.news'), route('news'));
});

Breadcrumbs::for('about', function ($trail) {
    $trail->parent('main');

    $trail->push(Lang::get('interface.about'), route('about'));
});