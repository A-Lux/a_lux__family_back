require('./bootstrap');
import Vue from 'vue';
import VueAxios from 'vue-axios';
import LibraryComponent from './components/LibraryComponent.vue';
import NewsComponent from './components/NewsComponent.vue';
import GalleryComponent from './components/GalleryComponent.vue';
import ContactsComponent from './components/ContactsComponent.vue';
import VueI18n from 'vue-i18n';
import messages from './i18n';

Vue.use(VueI18n);

if(document.getElementById('app')) {
    
    const i18n = new VueI18n({
        locale: 'en',
        messages: messages.messages,
    });

    Vue.use(VueAxios, axios);
    var vm = new Vue({
        i18n,
        el: '#app',
        components: {
            LibraryComponent,
            NewsComponent,
            GalleryComponent,
            ContactsComponent
        },
        created() {
            i18n.locale = document.documentElement.lang;
        },
        data() {
            return {
            }
        }
    })
}