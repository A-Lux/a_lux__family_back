const messages = {
    'kk': {
        address: 'Мекен-жайы',
        email: 'Электрондық пошта',
        phone: 'Телефон',
        schedule: 'Жұмыс уақыты',
        news: 'Жаңалықтар',
        show_more: 'Көбірек көрсету',
        show_all: 'Барлығын қарау',
        gallery: 'Галерея',
        choose_city: 'Қаланы таңдаңыз',
        library: 'Кітапхана',
        choose_chapter: 'Бөлімді таңдаңыз',
        come_to_us: 'Бізге келіңіз!',
        feedback_call: 'Кері қоңырау шалыңыз',
        leave_call: 'Өтініш қалдырыңыз, біздің мамандар сізбен байланысады',
        enter_name: 'Атыңызды енгізіңіз',
        leave_feedback: 'Сұрау жіберу',
        your_phone: 'Телефон нөміріңізді енгізіңіз',
        garant: 'Біз сіздің деректеріңіздің қауіпсіздігіне кепілдік береміз.',
        close: 'Жабу'
    },
    'ru': {
        address: 'Адрес',
        email: 'Электронная почта',
        phone: 'Телефон',
        schedule: 'Рабочее время',
        news: 'Новости',
        show_more: 'Показать еще',
        show_all: 'Показать все',
        gallery: 'Галерея',
        choose_city: 'Выберите город',
        library: 'Библиотека',
        choose_chapter: 'Выберите раздел',
        come_to_us: 'Приезжайте к нам!',
        feedback_call: 'Обратный звонок',
        leave_call: 'Оставьте заявку и с вами свяжутся наши специалисты',
        enter_name: 'Введите ваше имя',
        your_phone: 'Введите ваш телефон',
        leave_feedback: 'Отправить заявку',
        garant: 'Гарантируем безопасность ваших данных.',
        close: 'Закрыть'
    }
};

export default { messages };