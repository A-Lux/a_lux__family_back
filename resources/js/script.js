import swal from "sweetalert";

const requestForm = document.getElementById('request_form');
const requestFormButton = document.getElementById('request_form_button');
const requestFormName = requestForm ? requestForm.querySelector('input[name=name]') : null;
const requestFormEmail = requestForm ? requestForm.querySelector('input[name=email]') : null;
const mainNews = document.querySelectorAll('.cont-news .item');
const mainNewsHidden = document.querySelectorAll('.cont .col-lg-7 .item');

if(requestFormButton) {
    requestFormButton.addEventListener('click', () => {
        const data = new FormData(requestForm);
        requestFormName.nextElementSibling.innerHTML = '';
        requestFormEmail.nextElementSibling.innerHTML = '';

        axios.post('/api/requests', data)
            .then(r => {
                swal(r.data.message);
                requestFormName.value = '';
                requestFormEmail.value = '';
            })
            .catch(e => {
                const errors = e.response.data.errors;
                requestFormName.nextElementSibling.innerHTML = errors.name ? errors.name : '';
                requestFormEmail.nextElementSibling.innerHTML = errors.email ? errors.email :'';
            });
    });
}

if(mainNews.length > 0) {
    for (const news of mainNews) {
            news.addEventListener('click', e => {
                const id = news.getAttribute('data-id');
                for (const newsHidden of mainNewsHidden) {
                    newsHidden.classList = 'item d-none';
                }

                document.getElementById(id).classList = 'item d-block';
            });
    }
}
