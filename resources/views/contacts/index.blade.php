@extends('layouts.app')
@include('partials.meta')
@section('content')
    <section id="app" class="contacts">
        <div class="container">
            {{ Breadcrumbs::render('contacts') }}
            <div class="title-sec">@lang('interface.contacts')</div>
        </div>
        <contacts-component :region_id="{{  Request::input('region_id') ?: 'null' }}"></contacts-component>
    </section>
    @include('partials.request')
@endsection