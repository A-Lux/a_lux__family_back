@component('mail::message')
# Заявка из приложения Onege
<br>
от <a href="mailto:{{ $email }}">{{ $email }}</a>
<p>
    {{ $message }}
</p>
@endcomponent
