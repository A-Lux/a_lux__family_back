@extends('layouts.app')
@include('partials.meta')
@section('content')
<div>
    <div class="container">
    <div class="galety-content">
        <div class="py-3">
            <h1>@lang('interface.history')</h1>
            <div class="row">
                @foreach ($histories as $history)
                    <div class="col-md-6 col-xl-4 mb-4">
                        <div class="video">
                            {!! $history->iframe !!}
                        </div>
                    </div> 
                @endforeach
          </div>
        </div>
    </div>
</div>
</div>
@endsection