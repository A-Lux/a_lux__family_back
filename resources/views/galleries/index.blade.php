@extends('layouts.app')
@include('partials.meta')
@section('content')
    <div id="app" class="container">
        {{ Breadcrumbs::render('galleries') }}
        <gallery-component></gallery-component>
    </div>
    @include('partials.request')
@endsection