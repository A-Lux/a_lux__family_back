@extends('layouts.app')
@section('content')
<div>
    <div class="container">
    <div class="galety-content">
        <div>
            <div class="col-xl-6 p-0">
                <div class="galery-content-title">
                <ul>
                    <li><b>{{ $gallery->getModel()->getPostedAt(\App::getLocale()) }}</b></li>
                    <li><h4>{{ $gallery->title }}</h4></li>
                </ul>
            </div>
            </div>
            <br>
            <div class="zoom-gallery">
                <div class="row">
                    @foreach ($gallery->image as $image)
                        <div class="col-xl-3" style="max-height: 128px;overflow: hidden;">
                            <a href="{{ asset('storage/'.$image) }}">
                                <img src="{{ asset('storage/'.$image) }}">
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection