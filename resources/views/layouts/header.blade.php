<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]>
<html var extracted1="item__banner--first" ; class="ie ie7" lang="en">
<![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="en">
<![endif]-->
<!--[if (gte IE 9)|!(IE)]>
<!-->
<html lang="{{ \App::getLocale() }}">
<!--<![endif]-->

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-152876811-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-152876811-1');
    </script>
    <meta charset="utf-8">

    <title>@yield('title')</title>
    <meta name="description" content="@yield('description')">
    
    <link rel="shortcut icon" href="{{ asset('/img/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{ asset('/img/apple-touch-icon.png') }}">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="{{ asset('/libs/owlcarousel/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/libs/owlcarousel/assets/owl.theme.default.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/magnific-popup.css') }}">
    <meta name="host" content="{{ url('/') }}">
</head>


<body>

<!--Начало хедера-->    
<header>
    <div class="container"> 
        <div class="row">
            <div class="col-12 col-sm-11 col-lg-7 col-xl-7  cont-logo">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-3 logo-company">
                        <div class="img">
                            <a href="{{ url('/') }}"><img src="{{ asset('/img/logo1.png') }}"></a>
                        </div>
                        <div class="title">Социальный проект</div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3 logo-company">
                        <div class="img">
                        <a href="http://happychild.kz/">
                           <img src="{{ asset('/img/logo2.png') }}" alt="">
                        </a>
                        </div>
                        <div class="title">Общественный фонд "Семейная академия"</div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3 logo-company">
                            <div class="img">
                            <a href="http://qogam.gov.kz">
                                    <img src="{{ asset('/img/logo3.png') }}" alt="">
                            </a>
                            </div>
                        <div class="title">Министерство Информации и общественного  развития РК</div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-3 logo-company">
                            <div class="img">
                            <a href="https://cisc.kz/ru/">
                            <img src="{{ asset('/img/logo4.png' ) }}" alt="">
                            </a>
                            </div>
                        <div class="title">Центр поддержки гражданских инициатив</div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5 col-md-6 col-lg-5 cont-search">
                    <div class="row">
                            <div class="select">
                                <form method="GET">
                                    <select name="lang" onchange="this.parentNode.action = this.value; this.parentNode.submit();">
                                        <option value="{{ url('/lang') }}/kk" {{ \App::getLocale() == 'kk' ? 'selected' : ''}}>KZ</option>
                                        <option value="{{ url('/lang') }}/ru" {{ \App::getLocale() == 'ru' ? 'selected' : ''}}>RU</option>
                                    </select>
                                </form>
                            </div>
                            <div class="header_link">
                            <a href="mailto:{{ setting('site.email') }}" ><img src="{{ asset('/img/mail.png') }}" alt="">{{ setting('site.email') }}</a>
                            </div>
                    </div>
            </div>
            <a href="" class="mobile-menu">
                <span></span>
                <span></span>
                <span></span>
            </a>
        </div>
    </div>
    <div class="container nav-menu"> 
      <div class="row">
        <div class="col-sm-9 p-0">
            <ul>
                @foreach(menu('site', '_json') as $item)
                @php
                    $item = $item->translate(\App::getLocale());
                @endphp
                    @if($item->children->count() > 0)
                    <li>
                        <a href="#" class="accordion-trigger"> {{ $item->title }} <img src="{{ asset('img/bottom-arrow.png') }}"></a>
                        <ul class="sub-menu">
                        @foreach ($item->children as $sub_item)
                            @php
                                $sub_item = $sub_item->translate(\App::getLocale());
                            @endphp
                            <li><a href="{{ url($sub_item->url) }}">{{ $sub_item->title }}</a></li>
                        @endforeach
                        </ul>
                    </li>
                    @else
                    <li><a href="{{ url($item->url) }}"> {{ $item->title }}</a></li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="col-sm-3 p-0">
            <div class="soc-net">
                <a href="{{ setting('site.facebook') }}">
                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="30px" viewBox="0 0 96.124 96.123" xml:space="preserve"><g><g>
                    <path d="M72.089,0.02L59.624,0C45.62,0,36.57,9.285,36.57,23.656v10.907H24.037c-1.083,0-1.96,0.878-1.96,1.961v15.803   c0,1.083,0.878,1.96,1.96,1.96h12.533v39.876c0,1.083,0.877,1.96,1.96,1.96h16.352c1.083,0,1.96-0.878,1.96-1.96V54.287h14.654   c1.083,0,1.96-0.877,1.96-1.96l0.006-15.803c0-0.52-0.207-1.018-0.574-1.386c-0.367-0.368-0.867-0.575-1.387-0.575H56.842v-9.246   c0-4.444,1.059-6.7,6.848-6.7l8.397-0.003c1.082,0,1.959-0.878,1.959-1.96V1.98C74.046,0.899,73.17,0.022,72.089,0.02z" data-original="#000000" class="active-path"/>
                </g></g></svg>
            </a>
            <a href="{{ setting('site.instagram') }}">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" width="30px" viewBox="0 0 97.395 97.395" xml:space="preserve"><g><g>
                        <path d="M12.501,0h72.393c6.875,0,12.5,5.09,12.5,12.5v72.395c0,7.41-5.625,12.5-12.5,12.5H12.501C5.624,97.395,0,92.305,0,84.895   V12.5C0,5.09,5.624,0,12.501,0L12.501,0z M70.948,10.821c-2.412,0-4.383,1.972-4.383,4.385v10.495c0,2.412,1.971,4.385,4.383,4.385   h11.008c2.412,0,4.385-1.973,4.385-4.385V15.206c0-2.413-1.973-4.385-4.385-4.385H70.948L70.948,10.821z M86.387,41.188h-8.572   c0.811,2.648,1.25,5.453,1.25,8.355c0,16.2-13.556,29.332-30.275,29.332c-16.718,0-30.272-13.132-30.272-29.332   c0-2.904,0.438-5.708,1.25-8.355h-8.945v41.141c0,2.129,1.742,3.872,3.872,3.872h67.822c2.13,0,3.872-1.742,3.872-3.872V41.188   H86.387z M48.789,29.533c-10.802,0-19.56,8.485-19.56,18.953c0,10.468,8.758,18.953,19.56,18.953   c10.803,0,19.562-8.485,19.562-18.953C68.351,38.018,59.593,29.533,48.789,29.533z" data-original="#000000" class="active-path"/>
                    </g></g> </svg>                    
            </a>
            <a href="{{ setting('site.youtube') }}">
                <svg version="1.1" id="Layer_1"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px"
                viewBox="0 0 310 310" xml:space="preserve">
                    <g id="XMLID_822_">
                    <path id="XMLID_823_" d="M297.917,64.645c-11.19-13.302-31.85-18.728-71.306-18.728H83.386c-40.359,0-61.369,5.776-72.517,19.938
                    C0,79.663,0,100.008,0,128.166v53.669c0,54.551,12.896,82.248,83.386,82.248h143.226c34.216,0,53.176-4.788,65.442-16.527
                    C304.633,235.518,310,215.863,310,181.835v-53.669C310,98.471,309.159,78.006,297.917,64.645z M199.021,162.41l-65.038,33.991
                    c-1.454,0.76-3.044,1.137-4.632,1.137c-1.798,0-3.592-0.484-5.181-1.446c-2.992-1.813-4.819-5.056-4.819-8.554v-67.764
                    c0-3.492,1.822-6.732,4.808-8.546c2.987-1.814,6.702-1.938,9.801-0.328l65.038,33.772c3.309,1.718,5.387,5.134,5.392,8.861
                    C204.394,157.263,202.325,160.684,199.021,162.41z"/></g>
                </svg>
            </a>
            </div>
        </div>
    </div> 
</div>
</header>

<!--Конец хедера-->
