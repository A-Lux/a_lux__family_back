
@if (count($breadcrumbs))
    <div class="link">
        <ul>
            @foreach ($breadcrumbs as $breadcrumb)

                @if ($breadcrumb->url && !$loop->last)
                    <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                    <li><img src="images/right-arrow.png" alt=""></li>
                @else
                    <li><a href="">{{ $breadcrumb->title }}</a></li>
                @endif

            @endforeach
        </ul>
    </div>
@endif