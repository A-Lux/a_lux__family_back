<section class="offer" id="offer">
    <div class="container">
        <div class="col-md-8 col-xl-6 col-sm-12">
            <form id="request_form" method="POST">
                <div class="text">
                    <div class="title">@lang('interface.have_q')
                        <br>@lang('interface.leave_r')</div>
                    <div class="subtitle">@lang('interface.answer')</div>
                </div>
                <a href="tel:{{ setting('site.phone') }}">{{ setting('site.phone') }}</a>
            </form>
        </div>
    </div>
    <div class="flowers">
        <img src="{{ asset('/img/Layer 1.png') }}">
    </div>
</section>