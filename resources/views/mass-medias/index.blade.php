@extends('layouts.app')
@include('partials.meta')
@section('content')
<div class="container pt-5">
    <div class="mass-media-about-us">
        {{ Breadcrumbs::render('mass-medias') }}
        <div class="title">
            <h1>@lang('interface.mass_media')</h1>
        </div>
        @foreach ($articles as $article)
            <div class="library-card">
                <div class="row no-gutters">
                    <div class="col-md-4">
                        <img src="{{ asset('storage/'.$article->image) }}" class="card-img" alt="{{ $article->title }}">
                    </div>
                    <div class="col-md-8 pb-3">
                        <div class="card-body">
                            <p class="card-text"><small class="text-muted">@lang('interface.for') {{ $article->interviewed_by }}</small></p>
                            <h5 class="card-title mb-5">{{ $article->title }}</h5>
                            <a class="mt-5" href="{{ route('article', $article->id) }}">@lang('interface.follow')</a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
@include('partials.request')
@endsection