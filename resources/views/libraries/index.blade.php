@extends('layouts.app')
@include('partials.meta')
@section('content')
    <div id="app" class="container">
        {{ Breadcrumbs::render('libraries') }}
        <library-component></library-component>
    </div>
    @include('partials.request')
@endsection