@extends('layouts.app')
@include('partials.meta')
@section('content')
<div class="container">
    <div class="library-inside">
        <p class="text-muted">{{ $article->getModel()->getCreatedAt() }} {{ $article->interviewed_by ? ' для ' . $article->interviewed_by : '' }}</p>
        <h1>{{ $article->title }}</h1>
        <p class="text-muted">{{ $article->sub_title }} </p>
        <br>
        <img src="{{ asset('storage/'.$article->image) }}" alt="{{ $article->title }}" class="library-main-img">
        <br>
        <br>
        <p>
            {!! $article->content !!}
        </p>
    </div>
</div>
@include('partials.request')
@endsection