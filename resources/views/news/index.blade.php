@extends('layouts.app')
@include('partials.meta')
@section('content')
    <section class="page-news" id="app">
        <div class="container">
            {{ Breadcrumbs::render('news') }}
            <div class="title-sec">@lang('interface.news')</div>
        </div>
        <news-component></news-component>
    </section>
    @include('partials.request')
@endsection