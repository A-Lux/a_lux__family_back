<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use Translatable;
    protected $translatable = ['title'];
   
    public function regions()
    {
        return $this->belongsToMany(\App\Region::class, 'region_contact');
    }
}
