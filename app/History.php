<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    use Translatable;
    protected $translatable = ['title'];
    
    public function getDate() {
        $date = Carbon::parse($this->date)->locale(App::getLocale());
        return $date->day. ' '. ucfirst($date->monthName) . ' '.$date->year;
    }

    public function languages() {
        return $this->belongsToMany(\App\Language::class);
    }
}
