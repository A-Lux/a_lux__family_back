<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use Translatable;
    protected $translatable = ['title', 'sub_title', 'content'];
    protected $fillable = ['views'];
    
    public function category() {
        return $this->belongsTo(\App\Category::class);
    }

    public function getCreatedAt($lang = null) {
        $date = Carbon::parse($this->posted_at)->locale($lang ?: App::getLocale());
        return $date->day. ' '. ucfirst($date->monthName) . ' '.$date->year;
    }

    public function languages() {
        return $this->belongsToMany(\App\Language::class, 'language_article');
    }
}
