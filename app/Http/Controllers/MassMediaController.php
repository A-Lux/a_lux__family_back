<?php

namespace App\Http\Controllers;

use App\Meta;
use App\Article;
use Illuminate\Http\Request;

class MassMediaController extends Controller
{
    public function index(Request $request) {
        $meta = Meta::select('title', 'description')->where('page_id', 3)->first();

        $articles = Article::where('type', 3)->orderBy('position', 'asc')->get();

        foreach($articles as $key => $article) {
            $article->posted_at = $article->getCreatedAt($request->lang);
            $articles[$key] = $article->translate($request->lang);
        }

        return view('mass-medias.index', compact('articles', 'meta'));
    }
}
