<?php

namespace App\Http\Controllers\Api;

use App\Center;
use App\Region;
use App\Contact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function index() {
        $contacts = Contact::get();
        return response($contacts, 200);
    }

    public function regions(Contact $contact) {
        return response($contact->load('regions'), 200);
    }

    public function centers($contact_id, $region_id) {
        $centers = Center::where('contact_id', $contact_id)
            ->where('region_id', $region_id)
            ->get();
        $centers = $centers->map(function($v, $i){
            $v->info = $v->information()
                ->select(['id', 'center_id', 'key', 'value'])
                ->get()
                ->groupBy('key');
            unset($v->information);
            return $v;
        });
        return response($centers, 200);
    }
}
