<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class YoutubeController extends Controller
{
    public function index(Request $request) {
        $client = new Client();
        $response = $client->request(
                    'GET',
                    'https://www.googleapis.com/youtube/v3/search',
                    [
                        'query' => [
                            'key' => env('YOUTUBE_DATA_API_KEY'),
                            'channelId' => env('YOUTUBE_CHANNEL_ID'),
                            'part' => 'snippet,id',
                            'order' => 'date',
                            'maxResults' => $request->maxResults ?: 20
                        ]
                    ]
                );

        $headers = $response->getHeaders();
        $body = json_decode($response->getBody(), true);

        return response($body, 200);
    }
}