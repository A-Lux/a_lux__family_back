<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OneSignalPlayer;

class OneSignalController extends Controller
{
    public function store(Request $request) {
        $this->validate($request, [
            'player_id' => 'required'
        ]);
        
        if(!OneSignalPlayer::where('player_id', $request->player_id)->exists()) {
            OneSignalPlayer::create($request->all());
        }

        return response(200);
    }

    public function destroy(Request $request, $player_id) {
        $player = OneSignalPlayer::where('player_id', $player_id)->first();
        if($player) {
            $player->delete();
        }

        return response()->noContent();
    }
}