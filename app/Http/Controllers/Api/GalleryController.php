<?php

namespace App\Http\Controllers\Api;

use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GalleryController extends Controller
{
    public function index(Request $request, $region_id = null) {
        if($region_id) {
            $galleries = Gallery::where('region_id', $region_id)
                ->limit(8)
                ->get();
            
        } else {
            $galleries = Gallery::limit(8);
            $galleries = $galleries
                ->get();
        }

        foreach($galleries as $key => $gallery) {
            $gallery->image = count(json_decode($gallery->image, true)) > 0 ? array_slice(json_decode($gallery->image, true), 0, 8) : [];
            $gallery->original_title = $gallery->title;
            $gallery->posted = $gallery->getPostedAt($request->lang);
            $galleries[$key] = $gallery->translate($request->lang);
        }

        return response($galleries, 200);
    }
}
