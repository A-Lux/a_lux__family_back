<?php

namespace App\Http\Controllers\Api;

use App\Article;
use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ArticleController extends Controller
{
    public function index(Request $request) {
        if($request->has('ids')) {
            return response(Article::select(['id', 'title'])->whereIn('id', explode(',', $request->ids))->get(), 200);
        }

        if(!$request->has('lang')) {
            return response('Pass the lang parameter', 403);
        }

        $articlesQuery = Language::where('name', $request->lang)
            ->first()
            ->articles()
            ->where('type', 1);
        
        if($request->lastDate) {
            $articles = $articlesQuery->where('articles.created_at', '<', $request->lastDate)->limit(8)->orderBy('articles.created_at', 'desc')->get();
        }

        $articles = $articlesQuery->limit(8)->orderBy('articles.created_at', 'desc')->get();
        $lastKey = $articles->keys()->last();
        $lastItem = null;
        $nextPosition = null;
        
        if($lastKey !== null) {
            $lastItem = $articles[$lastKey];
        }
        
        if($lastItem) {
            $lastItemDate = $articles[$lastKey]['created_at'];

            $nextDate = $articlesQuery->where('articles.created_at', '<', $lastItemDate)->where('articles.id', '!=', $articles[$lastKey]['id'])->first();

            if($nextDate) {
                $nextDate = $nextDate->created_at;
            }
        }
        foreach($articles as $key => $article) {
            $article->posted_at = $article->getCreatedAt($request->lang);
            $articles[$key] = $article->translate($request->lang);
        }

        return response(['articles' => $articles, 'next_date' => $nextDate], 200);
    }

    public function show(Article $article) {
        return response($article, 200);
    }

    public function addView(Article $article) {
        $article->increment('views');

        return response(['views' => $article->views]);
    }
}
