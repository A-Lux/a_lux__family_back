<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function setting($setting) {
        if(setting('site.'.$setting)) {
            return response([$setting => setting('site.'.$setting)]);
        }else {
            return response('Not found', 404);
        }
    }
}
