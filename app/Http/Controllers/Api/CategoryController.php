<?php

namespace App\Http\Controllers\Api;

use App\Article;
use App\Category;
use App\Language;
use App\CategoryEntity;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    public function index(Request $request) {
        if($request->category_type) {
            $ids = CategoryEntity::select(['category_id'])->where('entity_id', $request->category_type)->get()->pluck('category_id');
            $categories = Category::select(['id', 'title', 'thumbnail', 'url', 'parent_id', 'created_at'])->whereIn('id', $ids);
        }else {
            $categories = Category::select(['id', 'title', 'thumbnail', 'url', 'parent_id', 'created_at']);
        }

        $categories = $categories->with('parent', 'children')
            ->orderBy('created_at', 'ASC')
            ->get();
        
        foreach($categories as $key => $category) {
            $categories[$key] = $category->translate($request->lang);
            $categories[$key]['children'] = $categories[$key]->getModel()->children;
        }

        return response($categories, 200);
    }

    public function articles(Category $category) {
        $articles = Article::where('category_id', $category->id)->get();
        return response($articles, 200);
    }

    public function libraries(Request $request, Category $category) {
        $articlesQuery = Language::where('name', $request->lang)->first();

        if($category->type == 2) {
            $articlesQuery = $articlesQuery->articles()
                ->where('category_id', $category->id)
                ->where('position', '!=', 0)
                ->where('type', 2);

            if($request->lastPosition) {
                $articles = $articlesQuery
                    ->where('articles.position', '>', $request->lastPosition);
            }

            $articles = $articlesQuery->limit(8)->orderBy('position', 'asc')->orderBy('articles.id', 'desc')->get();
            $lastKey = $articles->keys()->last();
            $lastItem = null;
            $nextPosition = null;

            if($lastKey !== null) {
                $lastItem = $articles[$lastKey];
            }

            if($lastItem) {
                $lastItemPosition = $articles[$lastKey]['position'];
                
                $nextPosition = $articlesQuery->where('articles.position', '>', $lastItemPosition)->where('articles.id', '!=', $articles[$lastKey]['id'])->first();
                if($nextPosition) {
                    $nextPosition = $nextPosition->position;
                }
            }

            foreach($articles as $key => $article) {
                $article->posted_at = $article->getCreatedAt($request->lang);
                $articles[$key] = $article->translate($request->lang);
            }

            return response(['articles' => $articles, 'next_position' => $nextPosition], 200);
        }

        return response('No such library category', 404);
    }
}
