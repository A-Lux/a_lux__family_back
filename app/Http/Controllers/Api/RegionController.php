<?php

namespace App\Http\Controllers\Api;

use App\Center;
use App\Region;
use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegionController extends Controller
{
    public function index(Request $request) {
        if($request->ownOffices) {
            $ids = Center::distinct()->select(['region_id'])->whereNull('contact_id')->get()->pluck('region_id');
            $regions = Region::select('id', 'name')->whereIn('id', $ids)->get();
        } elseif($request->forGallery) {
            $ids = Gallery::distinct()->select(['region_id'])->get()->pluck('region_id');
            $regions = Region::select('id', 'name')->whereIn('id', $ids)->get();
        } 
        else {
            $regions = Region::select('id', 'name')->get();
        }

        foreach($regions as $key => $region) {
            $regions[$key] = $region->translate($request->lang);
        }

        return response($regions, 200);
    }

    public function show(Region $region) {
        return response($region, 200);
    }

    public function centers(Request $request, $region_id) {
        if($request->ownOffices) {
            $center = Center::where('region_id', $region_id)
                ->whereNull('contact_id')
                ->first();
            if($center) {
                $center->info = $center->information()
                    ->select(['id', 'center_id', 'key', 'value'])
                    ->get()
                    ->groupBy('key');
                
                unset($center->information);
                
            }
            return response($center, 200);
        }

        $centers = Center::where('region_id', $region_id)
            ->get();
        $centers = $centers->map(function($v, $i){
            $v->info = $v->information()
                ->select(['id', 'center_id', 'key', 'value'])
                ->get()
                ->groupBy('key');
            unset($v->information);
            return $v;
        });
        
        return response($centers, 200);
    }
}
