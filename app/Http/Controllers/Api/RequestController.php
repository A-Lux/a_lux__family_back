<?php

namespace App\Http\Controllers\Api;

use App\EmailRequest;
use App\PhoneRequest;
use App\Mail\EmailRequest as ERequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class RequestController extends Controller
{
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email'
        ]);

        EmailRequest::create($request->all());

        return response(['message' => 'Заявка отправлена'], 200);
    }

    public function phone(Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'phone' => 'required',
            'center_id' => 'required|integer'
        ]);

        PhoneRequest::create($request->all());

        return response(['message' => 'Заявка отправлена'], 200);
    }

    public function email(Request $request) {
        $validatedData = $request->validate([
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required'
        ]);

        if(setting('site.email')) {
            Mail::to(setting('site.email'))->send(new ERequest($validatedData));
        }

        return response(['message' => 'Заявка отправлена'], 200);
    }
}
