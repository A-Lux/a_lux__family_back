<?php

namespace App\Http\Controllers;

use App\Meta;
use App\Page;
use App\Block;
use App\Slider;
use App\Article;
use App\History;
use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class MainController extends Controller
{
    public function index() {
        $articles = Language::where('name', \App::getLocale())
            ->first()
            ->articles()
            ->where('type', 1);

        $meta = Meta::select('title', 'description')->where('page_id', 1)->first();
        $sliders = Slider::get();
        foreach($sliders as $key => $slider) {
            $sliders[$key] = $slider->translate(App::getLocale());
        }
        $news = $articles->limit(8)->orderBy('articles.created_at', 'desc')->get();
        $histories = Language::where('name', \App::getLocale())
            ->first()
            ->histories()
            ->where('is_active', 1)->get();
        
        $page = Page::where('title', 'Главная')->first();
        $blocks = $page->blocks;
        return view('main.index', compact('sliders', 'news', 'histories', 'blocks', 'meta'));
    }
}