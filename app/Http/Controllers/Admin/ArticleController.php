<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\OneSignalPlayer;
use Illuminate\Http\Request;
use App\Events\ArticleCreated;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class ArticleController extends VoyagerBaseController
{

    public function store(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('add', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows)->validate();
        $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());
        
        event(new BreadDataAdded($dataType, $data));
        if($data->category->entities()->where('entity_id', 1)->exists()) {
            $player_ids = OneSignalPlayer::select('player_id')->get()->pluck('player_id');

            $push_data = [
                'article_id' => $data->id,
                'title' => $data->title,
                'body' => $data->sub_title
            ];

            event(new ArticleCreated($player_ids, 'Новая статья!' , $push_data));
        }
        
        return redirect()
        ->route("voyager.{$dataType->slug}.index")
        ->with([
                'message'    => __('voyager::generic.successfully_added_new')." {$dataType->getTranslatedAttribute('display_name_singular')}",
                'alert-type' => 'success',
            ]);
    }

    public function update(Request $request, $id) {
        parent::update($request, $id);
        
        $article = Article::find($id);
        Article::where('position', '>=', $article->position)
            ->where('type', $article->type)
            ->where('id', '!=', $article->id)
            ->update(['position' => DB::raw('position + 1')]);

        return redirect()
        ->route("voyager.articles.index")
        ->with([
            'message'    => "Статья #". $article->id ." успешно обновлена",
            'alert-type' => 'success',
        ]);
    }
}
