<?php

namespace App\Http\Controllers;

use App\Meta;
use App\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class ArticleController extends Controller
{
    public function show(Article $article) {
        if(!$article->languages()->where('name', \App::getLocale())->exists()) {
            return abort(404);
        }
        
        $meta = new Meta(['title' => $article->meta_title, 'description' => $article->meta_description]);
        $article = $article->translate(App::getLocale());
        return view('news.show', compact('article', 'meta'));
    }
}