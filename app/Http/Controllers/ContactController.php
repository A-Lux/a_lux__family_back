<?php

namespace App\Http\Controllers;

use App\Meta;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index() {
        $meta = Meta::select('title', 'description')->where('page_id', 7)->first();

        return view('contacts.index', compact('meta'));
    }
}
