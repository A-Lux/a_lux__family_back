<?php

namespace App\Http\Controllers;

use App\Meta;
use App\History;
use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class HistoryController extends Controller
{
    public function index(Request $request) {
        $histories = Language::where('name', App::getLocale())
            ->first()
            ->histories;
        
        $meta = Meta::select('title', 'description')->where('page_id', 8)->first();
        return view('histories.index', compact('histories', 'meta'));
    }
}
