<?php

namespace App\Http\Controllers;

use App\Meta;
use App\About;
use App\AboutProject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AboutController extends Controller
{
    public function index() {
        $meta = Meta::select('title', 'description')->where('page_id', 4)->first();
        $aboutProject = AboutProject::where('type', 0)->get();
        $projectsAims = AboutProject::where('type', 1)->get();
        $about = About::first();
        $about = $about ? $about->translate(App::getLocale()) : $about;
        return view('about-project.index', compact('aboutProject', 'projectsAims', 'about', 'meta'));
    }
}
