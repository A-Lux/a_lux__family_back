<?php

namespace App\Http\Controllers;

use App\Meta;
use App\Article;
use App\Category;
use Illuminate\Http\Request;

class LibraryController extends Controller
{
    public function index() {
        $meta = Meta::select('title', 'description')->where('page_id', 6)->first();

        return view('libraries.index', compact('meta'));
    } 
}
