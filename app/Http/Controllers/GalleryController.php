<?php

namespace App\Http\Controllers;

use App\Meta;
use App\Gallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class GalleryController extends Controller
{
    public function index() {
        $meta = Meta::select('title', 'description')->where('page_id', 5)->first();

        return view('galleries.index', compact('meta'));
    }

    public function show($date, $title) {
        $gallery = Gallery::where('posted_at', $date)->where('title', $title)->first();
        $gallery->image = json_decode($gallery->image);
        $gallery = $gallery->translate(App::getLocale());
        
        return view('galleries.show', compact('gallery'));
    }
}
