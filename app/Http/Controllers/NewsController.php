<?php

namespace App\Http\Controllers;

use App\Meta;
use App\Article;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    public function index() {
    $meta = Meta::select('title', 'description')->where('page_id', 2)->first();
        
        return view('news.index', compact('meta'));
    }
}
