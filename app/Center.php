<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Center extends Model
{
    use Translatable;
    protected $translatable = ['name', 'city'];
    
    public function region()
    {
        return $this->belongsTo(\App\Region::class);
    }

    public function contact()
    {
        return $this->belongsTo(\App\Contact::class);
    }

    public function information() {
        return $this->hasMany(\App\CenterInformation::class);
    }
}
