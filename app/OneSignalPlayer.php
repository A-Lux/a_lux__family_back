<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OneSignalPlayer extends Model
{
    protected $fillable = ['player_id'];
    
    public function getRouteKeyName()
    {
        return 'player_id';
    }
}
