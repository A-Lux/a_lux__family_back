<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class CenterInformation extends Model
{
    use Translatable;
    protected $translatable = ['key', 'value'];

    public function center() {
        return $this->belongsTo(\App\Center::class);
    }
}
