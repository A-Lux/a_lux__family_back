<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Translatable;
    protected $translatable = ['title'];
    
    public function parent() {
        return $this->belongsTo(\App\Category::class, 'parent_id');
    }

    public function children() {
        return $this->hasMany(\App\Category::class, 'parent_id');
    }

    public function articles() {
        return $this->hasMany(\App\Article::class); 
    }

    public function entities() {
        return $this->belongsToMany(\App\Entity::class, 'category_entity');
    }
}
