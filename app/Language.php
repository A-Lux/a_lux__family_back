<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Language extends Model
{
    public function articles() {
        return $this->belongsToMany(\App\Article::class, 'language_article');
    }

    public function histories() {
        return $this->belongsToMany(\App\History::class);
    }
}
