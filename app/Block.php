<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;


class Block extends Model
{
    use Translatable;
    
    protected $translatable = ['block_title', 'content'];
}
