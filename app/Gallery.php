<?php

namespace App;

use Carbon\Carbon;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Gallery extends Model
{
    use Translatable;
    protected $translatable = ['title'];
    
    public function getPostedAt($lang) {
        $date = Carbon::parse($this->posted_at)->locale($lang ?: App::getLocale());
        return $date->day. ' '. $date->monthName . ' '.$date->year;
    }
}
