<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class EmailRequest extends Model
{
    protected $fillable = ['name', 'email', 'center_id'];
}
