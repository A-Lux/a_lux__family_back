<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use Translatable;
    protected $translatable = ['name'];
    
    public function contacts()
    {
        return $this->belongsToMany(\App\Contact::class, 'region_contact');
    }
}
