<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;


class About extends Model
{
    
    use Translatable;

    protected $translatable = ['target_group', 'expected_results', 'right_text', 'text_under_image', 'bottom_text'];    
}
