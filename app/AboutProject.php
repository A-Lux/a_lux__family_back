<?php

namespace App;

use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;


class AboutProject extends Model
{
    use Translatable;

    protected $translatable = ['title', 'content'];    
}
