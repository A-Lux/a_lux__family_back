<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class PhoneRequest extends Model
{
    protected $fillable = [
        'name',
        'phone',
        'center_id'
    ];
}
