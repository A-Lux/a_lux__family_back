/* —---------------------------------------------------— */
$('.slider-story').owlCarousel({
    loop:true,
    autoplay:true,
    autoplayTimeout:5000,
    autoplayHoverPause:true,
    responsive:{
        0:{
            items:1
        },
        400:{
            items:1
        },
        500:{
            items:1
        },
        600:{
            items:2
        },
        900:{
            items:2
        },
        1000:{
            items:3
        },
        2000:{
            items:3
        }
    }
});

/* —---------------------------------------------------— */

/* Навигация по странице 
—---------------------------------------------------— */
$('.mobile-menu').click(function () {
    $('.mobile-window').addClass('nav-open');
    $('body').addClass('modal-open');
    return false;
});


$('.mobile-menu-close').click(function () {
    $('.mobile-window').removeClass('nav-open');   
    $('body').removeClass('modal-open');
    return false;
});


$(function () {
    $(document).click(function (event) {
        if ($('.mobile-window').hasClass('nav-open')) {
            if ($(event.target).closest('.mobile-window').length) {
                return;
            }
            $('.mobile-window').removeClass('nav-open');
            $('body').removeClass('modal-open');
            event.stopPropagation();
        }
    });
});

$('.accordion-trigger').click(function() { 
$(this).toggleClass('accordion-trigger--open') 
$(this).parent().find('.sub-menu').slideToggle(); 
});

$(document).ready(function() {
	$('.zoom-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		closeOnContentClick: false,
		closeBtnInside: false,
		mainClass: 'mfp-with-zoom mfp-img-mobile',
		image: {
			verticalFit: true,
			// titleSrc: function(item) {
			// 	return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
			// }
		},
		gallery: {
			enabled: true
		},
		zoom: {
			enabled: true,
			duration: 300, // don't foget to change the duration also in CSS
			opener: function(element) {
				return element.find('img');
			}
		}
		
	});
});
