<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'image' => 'https://picsum.photos/id/'.rand(1,200).'/200/200',
        'content' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'category_id' => factory(\App\Category::class)->create()->id
    ];
});
