<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'thumbnail' => 'https://picsum.photos/id/'.rand(1,200).'/200/200',
        'url' => $faker->name
    ];
});
