<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Center;
use Faker\Generator as Faker;

$factory->define(Center::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'contact_id' => factory(\App\Contact::class)->create()->id,
        'region_id' => factory(\App\Region::class)->create()->id
    ];
});
