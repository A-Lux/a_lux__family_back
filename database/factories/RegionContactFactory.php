<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\RegionContact;
use Faker\Generator as Faker;

$factory->define(RegionContact::class, function (Faker $faker) {
    return [
        'contact_id' => factory(\App\Contact::class)->create()->id,
        'region_id' => factory(\App\Region::class)->create()->id
    ];
});
